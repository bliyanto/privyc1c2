import { useReactMediaRecorder } from "react-media-recorder";
import { createFileName } from 'use-react-screenshot';
import moment from 'moment';

const Record = () => {
  const vidName = moment().format("DD-MM-YYYY hh:mm:ss") + " - Rec";
  const { status, startRecording, stopRecording, mediaBlobUrl} = useReactMediaRecorder ({video: true})
  const download = (video, { name = `${vidName}` , extension = 'mp4' } = {}) => {
    const a = document.createElement('a');
    a.href = mediaBlobUrl;
    a.download = createFileName(extension, name);
    a.click();
  };
  return (
    <div>
      <button className="btn" onClick={startRecording}>
        Start Video Recording
      </button>
      <button className="btn" onClick={stopRecording}>
        Stop Video Recording
      </button>
    <div>
    {mediaBlobUrl && (
      <button className="btn createRoom" onClick={download}>
      Download
      </button>
    )}
    </div>
    </div>
  )
};

export default Record